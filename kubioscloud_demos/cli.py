import json
import logging
from enum import Enum
from pathlib import Path
from typing import List, Optional, cast
from uuid import UUID

import typer
import yaml
from rich.console import Console

from . import auth, kubcloud

app = typer.Typer()
console = Console()


class MeasurementState(str, Enum):
    active = "active"
    finalized = "finalized"


@app.command(
    help="Login with user credentials. Store login data to tokens file (if specifid)."
)
def login(
    username: str = typer.Argument(None, envvar="KUB_USERNAME"),
    password: str = typer.Option(
        None, prompt=True, hide_input=True, envvar="KUB_PASSWORD"
    ),
    client_id: str = typer.Argument(None, envvar="KUB_CLIENT_ID"),
    tokens_file: Optional[Path] = typer.Option(
        None,
        file_okay=True,
        dir_okay=False,
        writable=True,
        readable=True,
        resolve_path=True,
        help="Authentication token storage file",
    ),
):
    uauth = auth.UserAuth(client_id=client_id)
    try:
        tokens = uauth.login(username, password)
    except auth.AuthenticationError as ex:
        console.log(f"[red]{ex}")
        return
    if tokens_file:
        console.log(f"Writing tokens to {tokens_file}")
        with tokens_file.open("w") as fh:
            yaml.dump(tokens, fh)
    console.log(json.dumps(tokens))


@app.command(help="Get user information")
def user(id_token: Optional[str] = typer.Argument(None, envvar="KUB_ID_TOKEN")):
    if not id_token:
        raise typer.BadParameter("Valid id_token is required.")
    user_info = kubcloud.user_info(id_token)
    console.log(user_info)


@app.command(help="List user's measurements")
def measurements_list(
    id_token: Optional[str] = typer.Argument(None, envvar="KUB_ID_TOKEN"),
    refresh_token: Optional[str] = None,
    state: Optional[MeasurementState] = None,
):
    if not id_token:
        raise typer.BadParameter("Valid id_token is required.")
    measurement_list = kubcloud.measurement_list(id_token)
    console.log(measurement_list)


@app.command(help="Get measurement details")
def measurement_info(
    measurement_id: UUID,
    id_token: Optional[str] = typer.Argument(None, envvar="KUB_ID_TOKEN"),
    refresh_token: Optional[str] = None,
):
    if not id_token:
        raise typer.BadParameter("Valid id_token is required.")
    measurement_info = kubcloud.measurement_info(
        measurement_id=measurement_id, id_token=id_token
    )
    console.log(measurement_info)


@app.command(help="Download measurement data")
def measurement_download(
    measurement_id: UUID,
    decode_data: bool = True,
    output: Optional[Path] = typer.Option(
        None,
        exists=False,
        file_okay=True,
        dir_okay=False,
        writable=True,
        readable=True,
        resolve_path=True,
        help="Output file",
    ),
    id_token: Optional[str] = typer.Argument(None, envvar="KUB_ID_TOKEN"),
    refresh_token: Optional[str] = None,
):
    if not id_token:
        raise typer.BadParameter("Valid id_token is required.")
    measurement_info = kubcloud.measurement_info(
        measurement_id=measurement_id, id_token=id_token
    )
    data = {}
    for ch in measurement_info["measure"].get("channels", []):
        data[ch["type"]] = kubcloud.get_channel_data(ch["data_url"], ch["data_enc"])
    if output:
        with output.open("w") as fh:
            json.dump(data, fh)
    else:
        console.log(data)


def channel_validation(channel_data: List[str]) -> List[kubcloud.ChannelSpec]:
    ch_specs = []
    for idx, ch_input in enumerate(channel_data):
        try:
            ch_data = json.loads(ch_input)
        except ValueError:
            raise typer.BadParameter(f"Malformatted json: '{ch_input}'")
        ch_data.setdefault("index", idx)
        try:
            ch_spec = kubcloud.ChannelSpec.from_spec(ch_data)
        except TypeError as ex:
            raise typer.BadParameter(f"Invalid channel specification: {ch_input}\n{ex}")
        ch_specs.append(ch_spec)
    return ch_specs


@app.command(help="Initialize new measurement")
def measurement_init(
    id_token: str,
    user_id: str = "self",
    description: Optional[str] = "",
    channel: List[str] = typer.Option(
        "",
        help="Channel specification json. Specify multiple channels by repeating argument.",
        callback=channel_validation,
    ),
):
    # Manual cast as MyPy doesn't recognize type change with typer option callback
    channels = cast(List[kubcloud.ChannelSpec], channel)
    response = kubcloud.measurement_init(
        id_token, user_id=user_id, description=description, channels=channels
    )
    console.log(response)


@app.command(help="Add data chunk to measurement")
def measurement_data(
    id_token: str,
    measurement_id: Optional[UUID] = None,
    data: str = "",
    data_file: Optional[Path] = typer.Option(
        None,
        exists=True,
        file_okay=True,
        dir_okay=False,
        writable=False,
        readable=True,
        resolve_path=True,
        help="Data file",
    ),
    chunk_seq: int = 0,
    channel_index: int = 0,
    user_id: str = "self",
):
    if not (data or data_file):
        console.log("No data given.")
        return
    values = []
    if data:
        values = json.loads(data)
    if data_file:
        with data_file.open("r") as fh:
            values = [float(line) for line in fh]
            if max(values) < 500:
                # Convert to ms
                values = [int(x * 1000) for x in values]
    if not measurement_id:
        console.log("No measurement_id given.")
        return
    response = kubcloud.measurement_chunk(
        id_token=id_token,
        user_id=user_id,
        measure_id=measurement_id,
        channel_index=channel_index,
        chunk_seq=chunk_seq,
        data=values,
    )
    console.log(response)


@app.command(help="Finalize measurement")
def measurement_final(
    id_token: str,
    measurement_id: Optional[UUID] = None,
    user_id: str = "self",
):
    response = kubcloud.measurement_final(
        id_token, measure_id=measurement_id, user_id=user_id
    )
    console.log(response)


@app.command(help="List user's results")
def results_list(
    id_token: Optional[str] = typer.Argument(None, envvar="KUB_ID_TOKEN"),
    refresh_token: Optional[str] = None,
):
    if not id_token:
        raise typer.BadParameter("Valid id_token is required.")
    results_list = kubcloud.results_list(id_token=id_token)
    console.log(results_list)


@app.callback(name="demo")
def main(
    ctx: typer.Context,
    config: Optional[Path] = typer.Option(
        None,
        exists=True,
        file_okay=True,
        dir_okay=False,
        writable=False,
        readable=True,
        resolve_path=True,
        help="Configuration file",
    ),
    tokens_file: Optional[Path] = typer.Option(
        None,
        exists=False,
        file_okay=True,
        dir_okay=False,
        writable=True,
        readable=True,
        resolve_path=True,
        help="Authentication token storage file",
    ),
    debug: bool = typer.Option(
        False, envvar="KUB_DEBUG", help="Verbose printing of requests made."
    ),
):
    """Common options"""
    subcommand = ctx.invoked_subcommand or ""
    if not subcommand:
        return
    for filepath in (config, tokens_file):
        if filepath is None or not filepath.exists():
            continue
        data = yaml.safe_load(filepath.open("r"))
        if ctx.default_map is None:
            ctx.default_map = {subcommand: {}}
        for param, value in data.items():
            ctx.default_map[subcommand].setdefault(param, value)
    if (
        subcommand == "login"
        and tokens_file is not None
        and ctx.default_map is not None
    ):
        ctx.default_map[subcommand].setdefault("tokens_file", tokens_file)
    if debug:
        logging.getLogger("kub_request").setLevel(logging.DEBUG)
