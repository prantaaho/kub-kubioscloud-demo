"""Unit tests for CLI functions"""
from unittest import mock

from typer.testing import CliRunner

from ..cli import app

runner = CliRunner()


@mock.patch("kubioscloud_demos.cli.auth")
def test_login_call(mockUserAuth):
    mock_uauth = mock.Mock()
    mock_uauth.login.return_value = {"id_token": "deadbeaf"}
    mockUserAuth.UserAuth.return_value = mock_uauth
    mockUserAuth.AuthenticationError = BaseException

    result = runner.invoke(app, ["--config", "sample_config.yaml", "login"])

    mockUserAuth.UserAuth.assert_called_with(client_id="<client_id>")
    mock_uauth.login.assert_called_with("<kubios_username>", "<password_for_the_user>")
    assert result.exit_code == 0
    assert "id_token" in result.stdout


@mock.patch("kubioscloud_demos.cli.kubcloud")
def test_measurement_init(mock_kubcloud):
    mock_kubcloud.measurement_init = mock.Mock(return_value="Ok")
    result = runner.invoke(
        app,
        [
            "measurement-init",
            "<id_token>",
            "--channel",
            '{"data_enc":[["value", "H"]], "label":"RR"}',
            "--channel",
            '{"data_enc":[["value", "H"]], "label":"RR2"}',
        ],
    )
    mock_kubcloud.measurement_init.assert_called_once_with(
        "<id_token>", user_id="self", description="", channels=[mock.ANY, mock.ANY]
    )
    assert result.exit_code == 0
