"""KubiosCloud authentication helpers"""
import logging
import os
import uuid
from typing import Dict

import requests

from .kubcloud import USER_AGENT, request_dump

deployment = os.environ.get("KUB_ENV", "prd")
if deployment == "prd":
    LOGIN_URL = "https://kubioscloud.auth.eu-west-1.amazoncognito.com/login"
    TOKEN_URL = "https://kubioscloud.auth.eu-west-1.amazoncognito.com/oauth2/token"
    REDIRECT_URI = "https://analysis.kubioscloud.com/v1/portal/login"
elif deployment == "stg":
    LOGIN_URL = "https://kubioscloud-stg.auth.eu-west-1.amazoncognito.com/login"
    TOKEN_URL = "https://kubioscloud-stg.auth.eu-west-1.amazoncognito.com/oauth2/token"
    REDIRECT_URI = "https://analysis.stg.kubioscloud.com/v1/portal/login"
else:
    raise ValueError("Unsupported Kubios Environment")


class AuthenticationError(BaseException):
    pass


class UserAuth:
    """Authentication using Authorization code grant"""

    def __init__(self, client_id: str, redirect_uri: str = REDIRECT_URI):
        """
        :param: client_id: Client ID
        """
        self.client_id = client_id
        self.redirect_uri = redirect_uri
        self.log = logging.getLogger(__name__)

    def login(self, username: str, password: str) -> Dict[str, str]:
        """Get authentication tokens using username & password.

        :param: username: KubiosCloud username
        :param: password: Password

        :return: dict with authentication and refresh tokens
        """
        csrf = str(uuid.uuid4())

        # Authentication
        session = requests.session()
        session.hooks["response"].append(request_dump)
        self.log.info(
            "Authenticating to %r with client_id: %r", LOGIN_URL, self.client_id
        )
        login_data = {
            "client_id": self.client_id,
            "redirect_uri": self.redirect_uri,
            "username": username,
            "password": password,
            "response_type": "code",
            "access_type": "offline",
            "_csrf": csrf,
        }
        login_response = session.post(
            LOGIN_URL,
            data=login_data,
            allow_redirects=False,
            headers={"Cookie": f"XSRF-TOKEN={csrf}", "User-Agent": USER_AGENT},
        )
        # Verify results
        if not login_response.status_code == 302:
            raise AuthenticationError(
                f"Status: {login_response.status_code}, Authentication failed."
            )
        code = login_response.headers["Location"].split("=")[1]
        self.log.debug("Got code: %r", code)

        # Exchanging code to tokens
        exch_data = {
            "client_id": self.client_id,
            "code": code,
            "redirect_uri": REDIRECT_URI,
            "grant_type": "authorization_code",
        }
        exch_response = session.post(TOKEN_URL, data=exch_data)
        self.log.debug("Status code %r", exch_response.status_code)
        tokens = exch_response.json()

        return tokens
